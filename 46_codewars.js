function deleteDigit(n) {
	n = n.toString();
	return [...Array(n.length)]
		.map((_, i) => +(n.slice(0, i) + n.slice(i + 1)))
		.sort((a, b) => b - a)[0];
}

console.log(deleteDigit(1356));