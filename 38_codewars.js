function sortArray(array) {
	const odd = array.filter(n => n % 2).sort((a, b) => a - b);
	let i = 0;
	return array.map(n => n % 2 ? odd[i++] : n);
}

console.log(sortArray([5, 3, 2, 8, 1, 4]));