function maskify(cc) {
	return cc.split('').map((el, i) => i < cc.length - 4 ? '#' : el).join('');
}

console.log(maskify('11111'));
