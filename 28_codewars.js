function scrollingText(text) {
	text = text.toUpperCase();
	const arr = [];
	for(let i = 0; i < text.length; i++) {
		arr.push(text.slice(i) + text.slice(0, i));
		// OR =>
		// arr.push(text);
		// text = text.slice(1, text.length) + text[0];
	}
	return arr;
}

console.log(scrollingText('Hello!'));
