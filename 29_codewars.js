function zipWith(fn,a0,a1) {
	return a0.map((el, i) => {
		if (a1[i] !== undefined) {
			return fn(el, a1[i]);
		}
	}).filter(el => el !== undefined);
}

console.log(zipWith(Math.pow, [10,10,10,10,10], [0,1,2,3]));
