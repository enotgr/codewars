function findEvenIndex(arr) {
	for (let i = 0; i < arr.length; i++) {
		if (arr.slice(0, i).sum() === arr.slice(i+1).sum()) return i;
	}
	return -1;
}

Array.prototype.sum = function() {
	let res = 0;
	this.forEach(el => res += el);
	return res;
};

console.log(findEvenIndex([20,10,-80,10,10,15,35]));
