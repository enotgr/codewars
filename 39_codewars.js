function list(names) {
	switch (names.length) {
		case 0:
			return '';
		case 1:
			return names[0].name;
		case 2:
			return names[0].name + ' & ' + names[1].name;
		default:
			return names
				.map(el => el.name)
				.slice(0, names.length - 1)
				.join(', ') + ' & ' + names[names.length - 1].name;
	}
}

console.log(list([ {name: 'Bart'}, {name: 'Lisa'}, {name: 'Maggie'} ]));