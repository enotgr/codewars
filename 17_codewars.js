function positiveSum(arr) {
	let res = 0;
	arr.forEach(el => {
		if (el > 0) {
			res += el;
		}
	});
	return res;
}

console.log(positiveSum([1,2,3,4,-5]));
