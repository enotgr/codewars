function balance(book) {
	bookArr = book.replace(/[!@#$%&*()_+=~{}:;?,]/g, '').split('\n');
	let originalBalance = +bookArr[0];
	let total = 0;
	bookArr = bookArr.filter(str => str.length).map((str, i) => {
		if (!i) return str;
		strArr = str.split(' ');
		const temp = strArr[2];
		originalBalance -= +temp;
		total += +temp;
		strArr.push('Balance', originalBalance.toFixed(2));
		return strArr.join(' ');
	});
	bookArr[0] = 'Original Balance: ' + bookArr[0];
	bookArr.push(`Total expense  ${total.toFixed(2)}`);
	bookArr.push(`Average expense  ${(total / (bookArr.length - 2)).toFixed(2)}`);
	return bookArr.join('\r\n');
}

const b1 = `1000.00!=
125 Market {,?!=:125.45
126 Hardware =34.95
127 Video! 7.45
128 Book :14.32
129 Gasoline ::16.10
`;

const b2 = `1233.00
125 Hardware 24.80
123 Flowers 93.50
127 Meat 120.90
120 Picture 34.00
124 Gasoline 11.00
123 Photos 71.40
122 Picture 93.50
132 Tyres 19.00
129 Stamps 13.60
129 Fruits 17.60
129 Market 128.00
121 Gasoline 13.60
`;

console.log(balance(b2));