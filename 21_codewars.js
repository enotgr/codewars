function digPow(n, p) {
	const s = (''+n)
		.split('')
		.map((n, i) => Math.pow(+n, p + i))
		.sum();
	return s % n ? -1 : s / n;
}

Array.prototype.sum = function() {
	let res = 0;
	this.forEach(el => {
		res += el;
	});
	return res;
};

console.log(digPow(46288, 3));
