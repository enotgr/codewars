function filterHomogenous(arrays) {
	return arrays.filter(arr => {
		if (!arr.length) return false;
		for (const el of arr) {
			if (typeof el !== typeof arr[0]) {
				return false;
			}
		}
		return true;
	});
}

// let filterHomogenous = a => a.filter(b => b.length > 0 && b.every(e => typeof e == typeof b[0])); //from codewars

console.log(filterHomogenous([[1, 5, 4], ['a', 3, 5], ['b'], [], ['1', 2, 3]]));
