const cake = (x, y) => {
	console.log(y
		.split('')
		.map((c, i) => i % 2 ? i + 1 : c.charCodeAt(0))
		.sum() / x);

	return +(y
	.split('')
	.map((c, i) => i % 2 ? i + 1 : c.charCodeAt(0))
	.sum() / x)
	.toFixed(2) >= 0.65 ? 'Fire!' : 'That was close!';
};

Array.prototype.sum = function() {
	let res = 0;
	this.forEach(el => res += el);
	return res;
};

console.log(cake(167, 'jk'));