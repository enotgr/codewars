// Create a function that takes an integer as an argument and returns "Even" for even numbers or "Odd" for odd numbers.
// Создайте функцию, которая принимает целое число в качестве аргумента и возвращает "Even" для четных чисел или "Odd" для нечетых.

function evenOddNumber(num) {
	return num % 2 ? "Odd" : "Even";
}

console.log(evenOddNumber(1));