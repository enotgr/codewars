function generateShape(n) {
	return ('+'.repeat(n) + '\n').repeat(n).slice(0, -1);
}

console.log(generateShape(5));