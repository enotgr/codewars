function proofread (str) {
	return str.split('. ').map(sentence => {
		const newSentence = sentence.toLowerCase().replace(/ie/g, 'ei');
		return newSentence[0].toUpperCase() + newSentence.slice(1);
	}).join('. ');
}

console.log(proofread("ThiEr DEcIEt wAs INconcIEVablE. sHe SIeZeD thE moMENT."));
