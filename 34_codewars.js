function box(n) {
	return [...Array(n)].map((_, i) => {
		if (!i || i === n - 1) {
			return ''.padStart(n, '-');
			// // OR
			// return [...Array(n).fill('-')].join('');
		}
		return '-' + ''.padStart(n - 2, ' ') + '-';
		// // OR
		// return [...Array(n)].map((__, j) => {
		// 	if (!j || j === n - 1) {
		// 		return '-';
		// 	}
		// 	return ' ';
		// }).join('');
	});
}

box(10).forEach(el => console.log(el));