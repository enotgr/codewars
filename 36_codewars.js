function smartSum(...arr) {
	let sum = 0;
	arr.forEach(el => {
		sum += el.length ? smartSum(...el) : el;
	});
	return sum;
}

console.log(smartSum(1, 2, [3, 4], 5, [6, [7, 8], 9]));