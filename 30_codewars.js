const reverseSeq = n => {
	return [...Array(n)].map((_, i) => i + 1).reverse();
	// // OR
	// const arr = [];
	// for (let i = n; i > 0; i--) {
	// 	arr.push(i);
	// }
	// return arr;
};

console.log(reverseSeq(5));
