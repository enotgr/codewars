function bingo(ticket, win) {
	let mini = 0;
	ticket.forEach(arr => {
		mini += +arr[0]
			.split('')
			.map(el => el.charCodeAt())
			.includes(arr[1]);
	});
	return mini >= win ? 'Winner!' : 'Loser!';
}

console.log(bingo([['HGTYRE', 74], ['BE', 66], ['JKTY', 74]], 3));
