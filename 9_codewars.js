function insertDash(num) {
	const numArray = num.toString().split('');
	for (let i = 1; i < numArray.length; i++) {
		if (+numArray[i] % 2 && +numArray[i-1] % 2) {
			numArray[i - 1] += '-';
		}
	}
	return numArray.join('');
}

console.log(insertDash(6633));
