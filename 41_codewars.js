//return the total number of smiling faces in the array
function countSmileys(arr) {
	let count = 0;
	arr.forEach(el => {
		if ((el[0] === ':' || el[0] === ';') &&
			(el[el.length - 1] === ')' || el[el.length - 1] === 'D') &&
			(el.length === 2 || (el.length === 3 && (el[1] === '-' || el[1] === '~')))
		) count++;
	});
	return count;
}

// function countSmileys(arr) {
// 	return arr.filter(x => /^[:;][-~]?[)D]$/.test(x)).length;
// }

console.log(countSmileys([';]', ':[', ';*', ':$', ';-D']));
