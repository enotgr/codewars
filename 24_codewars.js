function processArray(arr, callback) {
	return arr.map(callback); // it's equal like this: arr.map(el => callback(el))
}

console.log(processArray([1, 3, 5, 6, 7, 8], n => n*2));