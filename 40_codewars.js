function anagrams(word, words) {
	const arr = [];
	for (const el of words) {
		if (el.length !== word.length) continue;
		if (
			el.split('').map(n => n.charCodeAt()).reduce((acc, cur) => acc + cur) ===
			word.split('').map(n => n.charCodeAt()).reduce((acc, cur) => acc + cur)
		) {
			arr.push(el);
		}
	}
	return arr;
}

console.log(anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']));
