function arrayPlusArray(arr1, arr2) {
	return arr1.concat(arr2).sum();
}

Array.prototype.sum = function() {
	let res = 0;
	this.forEach(el => res += el);
	return res;
};

console.log(arrayPlusArray([100, 200, 300], [400, 500, 600]));