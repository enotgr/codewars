const format = (str, obj) => {
	let fills = '';
	for (const fill in obj) {
		fills += '\\{' + fill + '\\}|';
	}
	const expr = new RegExp(fills.slice(0, -1), 'g');
	str = str.replace(expr, match => obj[match.slice(1, -1)]);
	return str;
};

console.log(format('Hello {foo} - {foobar} make me a {bar}... {foo}!!?', { bar : 'sandwich {foo}', foo : 'Jack' }));
