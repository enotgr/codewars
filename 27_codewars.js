function rowWeights(array) {
	let command1 = 0;
	let command2 = 0;
	array.forEach((el, i) => {
		if (i % 2) {
			command2 += el;
		} else {
			command1 += el;
		}
	});
	return [command1, command2];
}

console.log(rowWeights([29,83,67,53,19,28,96]));
