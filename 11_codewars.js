function createPhoneNumber(numbers) {
	return '(' + numbers.slice(0, 3).join('') + ') ' + numbers.splice(3, 3).join('') + '-' + numbers.splice(-4, 4).join('');
}

console.log(createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]));