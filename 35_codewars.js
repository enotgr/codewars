// return the magic index
function findMagic(arr) {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] === i) return i;
	}
	return -1;
}

console.log(findMagic([6, 5, 83, 5, 3, 18]));
