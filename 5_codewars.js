function rgb(...colors) {
	return colors.map(color => {
		if (color > 255) return 'FF';
		if (color < 0) return '00';
		const mapNums = {
			10: 'A',
			11: 'B',
			12: 'C',
			13: 'D',
			14: 'E',
			15: 'F'
		};
		let div = Math.floor(color / 16);
		let mod = Math.floor(color % 16);
		div = div < 10 ? div : mapNums[div];
		mod = mod < 10 ? mod : mapNums[mod];

		return div.toString() + mod;
	})
	.join('');
}

console.log(rgb(0, 0, -20));
