// Task:
// Given a list of integers l, return the list with each value multiplied by integer n.

// Restrictions:
// The code must not:

// 1. contain *.
// 2. use eval() or exec()
// 3. contain for
// 4. modify l
// 5. Happy coding :)

// function multiply(n, l) {
// 	const newArr = [];
// 	let i = 0;
// 	while (i < l.length) {
// 		let j = 0;
// 		let sum = 0;
// 		if (n > 0) {
// 			while (j < n) {
// 				sum += l[i];
// 				j++;
// 			}
// 		} else {
// 			if (l[i] === 0) {
// 				newArr.push(-0);
// 				i++;
// 				continue;
// 			}
// 			while (j > n) {
// 				sum -= l[i];
// 				j--;
// 			}
// 		}
// 		newArr.push(sum);
// 		i++;
// 	}
// 	return newArr;
// }

function multiply(n, l) {
	return l.map(a => Math.round(a / (1 / n)));
}

let l = [0, 1, 2, 3];

console.log(multiply(-2, l));
