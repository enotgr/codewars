function getCount(str) {
	const vowels = ['a', 'e', 'i', 'o', 'u'];
	let vowelsCount = 0;

	str.split('').forEach(s => {
		if (vowels.includes(s)) vowelsCount++;
	});

	return vowelsCount;
 }

 console.log(getCount("abracadabra"));
