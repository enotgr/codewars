function findShort(s) {
	// return s.split(' ').sort((a, b) => a.length - b.length)[0].length; //it can be done like this
	let minLength = s.length;
	s.split(' ').forEach(el => {
		minLength = el.length < minLength ? el.length : minLength;
	});
	return minLength;
}

console.log(findShort('bitcoin take over the world maybe who knows perhaps'));
