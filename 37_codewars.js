const capitalize = str => [
	str.split('').map((s, i) => i % 2 ? s : s.toUpperCase()).join(''),
	str.split('').map((s, i) => i % 2 ? s.toUpperCase() : s).join('')
];

// // OR
// function capitalize(str) {
// 	const arr = ['', ''];
// 	str.split('').forEach((el, i) => {
// 		if (i % 2) {
// 			arr[0] += el;
// 			arr[1] += el.toUpperCase();
// 		} else {
// 			arr[0] += el.toUpperCase();
// 			arr[1] += el;
// 		}
// 	});
// 	return arr;
// }

console.log(capitalize("hellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohellohello"));