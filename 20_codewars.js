function squareDigits(num) {
	return +(num+'')
		.split('')
		.map(n => n*n)
		.join('');
}

console.log(squareDigits(9119));